﻿using GartnerAssignment.DataLayer;
using GartnerAssignment.ImportTypes;
using GartnerAssignment.Services;
using System;

namespace GartnerAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            ISaaSProductsImportService _saaSProductsImport = null;
            SampleInputOutput input = new SampleInputOutput();
            var importQueryArray = input?.GetInput();

            if (importQueryArray != null)
            {
                switch (importQueryArray[1]?.ToLower())
                {
                    case "capterra":
                        _saaSProductsImport = new SaaSProductsImportService(new CapterraImport(), new ProductRepository());
                        break;
                    case "softwareadvice":
                        _saaSProductsImport = new SaaSProductsImportService(new SoftwareAdviceImport(), new ProductRepository());
                        break;
                    default: break;
                }


                var saved = _saaSProductsImport.Import();
                if (saved)
                {
                    Console.WriteLine(importQueryArray[1] + " file imported successfully.");
                    Console.ReadKey(true);
                }
                else
                {
                    Console.WriteLine("Some error occurred.");
                    Console.ReadKey(true);
                }
            }
        }
    }
}

