﻿using GartnerAssignment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GartnerAssignment.ImportTypes
{
    public class SoftwareAdviceImport : IImportType
    {
        string actualPath;
        public SoftwareAdviceImport()
        {
            actualPath = SampleInputOutput.Directory + "\\feed-products\\softwareadvice.json";
        }
        public List<Product> Import()
        {
            //Not able to write correct parsing logic due to lack of time
            //string json = File.ReadAllText(actualPath);
            //var x = JsonConvert.SerializeObject(json);
            //var productList = JsonConvert.DeserializeObject<Product>(x);
            return new List<Product> { new Product { Name = "SoftwareAdvice Product"} };
        }
    }
}
