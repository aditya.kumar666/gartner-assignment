﻿using GartnerAssignment.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment.ImportTypes
{
    public class CapterraImport : IImportType
    {
        string actualPath;
        public CapterraImport()
        {
            actualPath = SampleInputOutput.Directory + "\\feed-products\\capterra.yaml";
        }
        public List<Product> Import()
        {
            return new List<Product> { new Product { Name = "Capterra Product" } };
        }
    }
}
