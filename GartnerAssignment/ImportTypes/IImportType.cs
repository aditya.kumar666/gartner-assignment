﻿using GartnerAssignment.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment.ImportTypes
{
    public interface IImportType
    {
        public List<Product> Import();
    }
}
