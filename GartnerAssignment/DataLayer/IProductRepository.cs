﻿using GartnerAssignment.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment.DataLayer
{
    public interface IProductRepository
    {
        public List<Product> AddProducts(List<Product> productList);
    }
}
