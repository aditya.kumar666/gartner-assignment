﻿using GartnerAssignment.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment.DataLayer
{
    //Not writing Db Context related code as given in assignment just creating dummy mocks
    public class ProductRepository : IProductRepository
    {
        public ProductRepository()
        {

        }

        public List<Product> AddProducts(List<Product> productList)
        {
            //it will return the added list after adding ProductList in database.
            return productList;
        }
    }
}
