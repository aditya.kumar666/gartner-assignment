﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string[] categories { get; set; }
        public string twitter { get; set; }
        public string title { get; set; }
        public string Tags { get; set; }
    }
}
