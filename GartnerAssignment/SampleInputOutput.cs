﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment
{
    public class SampleInputOutput
    {
        public static string Directory;

        public SampleInputOutput()
        {
            //fetch the directory of the DLL
            Directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        }

        public string[] GetInput()
        {
            Console.WriteLine(@"Please keep all files to import inside following path: "+ Directory + "\\feed-products\\");
            Console.WriteLine("Please write command to import.");
            var inputCommand = Console.ReadLine().Split(' ');
            if(inputCommand.Length == 3 && inputCommand[0] == "import")
            {
                return inputCommand;
            }
            else
            {
                Console.WriteLine("Please input right command.");
                Console.ReadKey(true);
                return null;
            }
        }
    }
}
