﻿using GartnerAssignment.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GartnerAssignment.Services
{
    interface ISaaSProductsImportService
    {
        public bool Import();
    }
}
