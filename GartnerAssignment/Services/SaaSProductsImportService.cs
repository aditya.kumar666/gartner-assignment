﻿using GartnerAssignment.DataLayer;
using GartnerAssignment.ImportTypes;
using GartnerAssignment.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace GartnerAssignment.Services
{
    public class SaaSProductsImportService : ISaaSProductsImportService
    {
        IImportType _importType;
        IProductRepository _productRepository;
        public SaaSProductsImportService(IImportType importType, IProductRepository productRepository)
        {
            _importType = importType;
            _productRepository = productRepository;
        }
        public bool Import()
        {
            if (_importType != null && _productRepository != null)
            {
                List<Product> productList = _importType.Import();
                return _productRepository.AddProducts(productList).Count() > 0;
            }
            else
            {
                return false;
            }
        }
    }
}
