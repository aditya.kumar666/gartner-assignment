﻿using GartnerAssignment.ImportTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class CapterraImportTest
    {
        IImportType _capterraImport;
        public CapterraImportTest()
        {
            _capterraImport = new CapterraImport();
        }

        [TestMethod]
        public void Import_TestImportingCapterraFile()
        {
            var result = _capterraImport.Import();

            Assert.IsNotNull(result);
            Assert.AreEqual(result.FirstOrDefault().Name, "Capterra Product");
        }
    }
}
