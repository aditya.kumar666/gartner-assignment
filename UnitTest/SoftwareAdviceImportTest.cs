﻿using GartnerAssignment.ImportTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class SoftwareAdviceImportTest
    {
        IImportType _softwareAdviceImport;
        public SoftwareAdviceImportTest()
        {
            _softwareAdviceImport = new SoftwareAdviceImport();
        }

        [TestMethod]
        public void Import_TestImportingCapterraFile()
        {
            var result = _softwareAdviceImport.Import();

            Assert.IsNotNull(result);
            Assert.AreEqual(result.FirstOrDefault().Name, "SoftwareAdvice Product");
        }
    }
}
