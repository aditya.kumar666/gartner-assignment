using GartnerAssignment.DataLayer;
using GartnerAssignment.ImportTypes;
using GartnerAssignment.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class SaasProductsImportServiceTest
    {
        SaaSProductsImportService _saaSProductsImportService;

        [TestMethod]
        public void Import_TestImportingCapterraFile()
        {
            _saaSProductsImportService = new SaaSProductsImportService(new CapterraImport(), new ProductRepository());

            var result = _saaSProductsImportService.Import();

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Import_TestImportingSoftwareAdviceFile()
        {
            _saaSProductsImportService = new SaaSProductsImportService(new SoftwareAdviceImport(), new ProductRepository());

            var result = _saaSProductsImportService.Import();

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Import_TestNulls()
        {
            _saaSProductsImportService = new SaaSProductsImportService(new SoftwareAdviceImport(), null);

            var result = _saaSProductsImportService.Import();

            Assert.IsTrue(result);
        }
    }
}
